package wulandari.afifah.appenam

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.perkalian.*
import java.text.DecimalFormat

class perkalian : AppCompatActivity(), View.OnClickListener{

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edx.text.toString().toDouble()
        y = edx.text.toString().toDouble()
        hasil = x*y
        txhasilkali.text = DecimalFormat("#.##").format(hasil)
    }
    override fun onCreate(savedInstanceState : Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.perkalian)
        btn.setOnClickListener(this)

        //cek jika banyak package dari parent activity
        var paket : Bundle? = intent.extras
        edx.setText(paket?.getString("x"))
    }

    override fun finish() {
        var  intent = Intent()

        intent.putExtra("hasilKali",txhasilkali.text.toString())
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }
}